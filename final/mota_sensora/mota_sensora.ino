#include <SimpleDHT.h> // DHT built-in library
#include <nRF905.h> // nRF905 radio module built-in library
#include <SPI.h> // aux library for SPI communication

#include <string.h> // for array coping

#include <DallasTemperature.h> // DS18B20 built-in library
#include <OneWire.h> // aux library for one wire communication

#define RXADDR {0xFE, 0x4C, 0xA6, 0xE5} // Radio address of this device (4 bytes)
#define TXADDR {0x58, 0x6F, 0x2E, 0x10} // Radio address of device to send to (4 bytes)

// Data wire is plugged into port 5 on the Arduino
#define ONE_WIRE_BUS 5
#define DHT11_PIN 4
#define LDR_PIN A0


#define TIMEOUT 1000 // 1 second ping timeout

// Setup a oneWire instance to communicate with any OneWire devices
OneWire oneWire(ONE_WIRE_BUS);
// Pass our oneWire reference to Dallas Temperature.
DallasTemperature sensors(&oneWire);

SimpleDHT11 dht11;

int  lightValue =  0;  // variable to  store 


void readDHT11(int dht11_data[]){
  byte temperature = 0;
  byte humidity = 0;
  if (dht11.read(DHT11_PIN, &temperature, &humidity, NULL)) {
    Serial.print("Read DHT11 failed");
    return;
  }

  Serial.println("");
  Serial.print("Sample OK: ");
  Serial.print((int)temperature); Serial.print(" *C, ");
  Serial.print((int)humidity); Serial.println(" %");
  
  dht11_data[0] = temperature;
  dht11_data[1] = humidity;
}

float readTemperature(){
  
  Serial.print("Requesting temperatures...");
  sensors.requestTemperatures(); // Send the command to get temperatures
  Serial.println("DONE");
  Serial.print("Temperature for the device 1 (index 0) is: ");
  Serial.println(sensors.getTempCByIndex(0));
  return sensors.getTempCByIndex(0);
}


float readLDR(){
  lightValue =  analogRead(LDR_PIN);
  Serial.println(lightValue,  DEC);
  float voltage = lightValue * (5.0 / 1023.0);
  Serial.println(voltage);
  return voltage;
}

void setup()
{
  // Start up
  nRF905_init();

  // Set address of this device
  byte addr[] = RXADDR;
  nRF905_setRXAddress(addr);

  // Put into receive mode
  nRF905_receive();

  // Start up the library
  sensors.begin();
  Serial.begin(9600);

  Serial.println(F("Client started"));
}


void loop()
{
  int dht11_data[2]; 

  readDHT11(&dht11_data[0]);
  float tempC = readTemperature();
  float luminosity = readLDR();
  

  // Make data
  byte data[NRF905_MAX_PAYLOAD];
  unsigned long startTime = millis();

  float dataTransfer[] = {tempC, dht11_data[1], luminosity};

  memcpy(data, dataTransfer, 3 * sizeof(float));

  // Set address of device to send to
  byte addr[] = TXADDR;
  nRF905_setTXAddress(addr);

  // Set payload data
  nRF905_setData(data, sizeof(data));

  // Send payload (send fails if other transmissions are going on, keep trying until success)
  while (!nRF905_send());

  // Put into receive mode
  nRF905_receive();

  // Make buffer for reply
  byte buffer[NRF905_MAX_PAYLOAD];
  bool success;

  // Wait for reply with timeout
  unsigned long sendStartTime = millis();
  while (1)
  {
    success = nRF905_getData(buffer, sizeof(buffer));
    if (success) // Got data
      break;

    // Timeout
    if (millis() - sendStartTime > TIMEOUT)
      break;
  }

  if (success)
  {
    unsigned int totalTime = millis() - startTime;

    Serial.print(F("Ping time: "));
    Serial.print(totalTime);
    Serial.println(F("ms"));

    // Printout ping contents
    Serial.print(F("Data from server: "));

  float serverResponse;
  memcpy(&serverResponse, buffer, sizeof(float));
    
    Serial.print(serverResponse);
    Serial.println();

  }
  else
    Serial.println(F("Ping timed out"));
    
  delay(1000); //Wait 1s between measures
}
