#include <SPI.h>
#include <nRF905.h>
#include <EEPROM.h>
#include <SoftwareSerial.h> //library for custom serial communication (using diferent pins)

#define RXADDR {0x58, 0x6F, 0x2E, 0x10} // Address of this device (4 bytes)
#define TXADDR {0xFE, 0x4C, 0xA6, 0xE5} // Address of device to send to (4 bytes)

SoftwareSerial BT(4,5); //4 RX, 5 TX.

int eepromAddr = 0;

void setup()
{
  // Start up
  nRF905_init();
  
  // Set address of this device
  byte addr[] = RXADDR;
  nRF905_setRXAddress(addr);

  // Put into receive mode
  nRF905_receive();
  Serial.begin(9600);
  Serial.println(F("Server started"));

  BT.begin(9600); //set bluetooth serial speed
}

void loop()
{
  Serial.println(F("Waiting for ping..."));

  // Make buffer for data
  byte buffer[NRF905_MAX_PAYLOAD];

  // Wait for data
  while(!nRF905_getData(buffer, sizeof(buffer)));

  Serial.println(F("Got ping"));

  // Set address of device to send to
  byte addr[] = TXADDR;
  nRF905_setTXAddress(addr);

  // Set payload data (reply with data received)
  nRF905_setData(buffer, sizeof(buffer));

  // Send payload (send fails if other transmissions are going on, keep trying until success)
  while(!nRF905_send());

  // Put back into receive mode
  nRF905_receive();

  union
  {
    float value;
    byte buf[4];
  } sensorValue;

  float sensorsMeasures[] = {0.0, 0.0, 0.0}; // Data is in format [ temp (C), humidity (%), luminosity (V)]
  
  for (int i = 0; i < 3; i++) {
    for(int j = 0; j < 4; j ++) {
      sensorValue.buf[j] = buffer[i * 4 + j];
    }
    Serial.println(sensorValue.value);
    sensorsMeasures[i] = sensorValue.value;
  }

  if(BT.available()){
    // Read data from EEPROM if stored
    if( eepromAddr > 0 ) {
      BT.print(F("ss"));
      for( int i = 0; i < eepromAddr; i += sizeof(float)){
        float oldValue = 0.00f;
        EEPROM.get(i, oldValue);
        BT.print(oldValue, sizeof(float));
        if( i % 12 == 0 ){
          BT.print(F("ee"));
          if(i < eepromAddr){
            BT.print(F("ss"));
          }
        }
      }
      eepromAddr = 0; //Start saving data from first again
    }

    // Write received data
    BT.print(F("ss"));
    for(int i = 0; i < 3; i++) {
      BT.print(sensorsMeasures[i]);
    }
    BT.print(F("ee"));
  } else {
    //Write to EEPROM until reaching capacity

    for(int i = 0; i < 3; i++) {
      if(addr < EEPROM.length()) {
        EEPROM.put(eepromAddr, sensorsMeasures[i]);
        eepromAddr += sizeof(float);
      }
    }
  }
}

